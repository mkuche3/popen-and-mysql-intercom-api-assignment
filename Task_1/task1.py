from subprocess import Popen
import time

#List of given commands
commands = [
   'sleep 3',
   'ls -l /',
   'find /',
   'sleep 4',
   'find /usr',
   'date',
   'sleep 5',
   'uptime'
]

#Dictionaries declaration
pidEndTimeDict = {}	#maintains pid and associated end time
cmdStartTimeDict = {}  #maintains commands and start time
pidCommandDict = {}    #maintains pid and commands 
executionTimeDict = {} #maintains execution time and commands

#Function to save start time
def extractStartTime(command):
	cmdStartTimeDict[command] = time.time()

#Report
def printReport():
	sumOfTime=0
	sortedTime = sorted(executionTimeDict)
	for item in sortedTime:	
		sumOfTime += item
		print("\n Command:  ", executionTimeDict[item],"\t Time:", item)
	print("\n Minimum_time: ",sortedTime[0])
	print("\n Maximum_time: ",sortedTime[-1])
	print("\n Avergae time: ",(sumOfTime/len(sortedTime)))
	print("\n Total elapsed time: ",end_time-start_time)


start_time = time.time()

# runs in parallel, preexec_fn only used for Unix based system
processes = [Popen([cmd], shell=True, preexec_fn = extractStartTime(cmd)) for cmd in commands]

#Function to save end time
def extractEndTime(p):
	if p.pid not in pidEndTimeDict:
		pidEndTimeDict[p.pid]=time.time()


#Extracts end time as soon as the subprocess ends
while True:
	endtime = [extractEndTime(p) for p in processes if p.poll() is not None]
	#Checks if all subprocesses are ended and their time is extracted
	if(len(endtime)==len(commands)):
		end_time = time.time()
		break


#Sort the Pids in ascending order, usually pids are assigned in sequential order so we map the sorted pids to given list of commands 
sortedPids = sorted(pidEndTimeDict)
i = 0
for eachID in sortedPids:
	pidCommandDict[commands[i]] = eachID
	i += 1

print("Profile:")
#Calculate and store execution time for each command
for eachCommand in commands:
	startTime = cmdStartTimeDict[eachCommand]
	endTime = pidEndTimeDict[pidCommandDict[eachCommand]]
	executionTimeDict[endTime - startTime] = eachCommand

printReport()







