import MySQLdb
import sys
import json
import requests

def connectDb():
    # open a database connection
    connection = MySQLdb.connect (host = "localhost", user = "root", passwd = "********", db = "database_name")
    cursor = connection.cursor ()
    cursor.execute ("select * from user") # execute the SQL query
    data = cursor.fetchall () # fetch all of the rows from the query result
    convertToJson(data) #call to convert to json  
    cursor.close ()     # close the cursor object
    connection.close () # close the connection    


#Function to convert data to json objects
def convertToJson(data):
    for row in data :
        userData = json.dumps({'id': row[0], 'name': row[1], 'email':row[2]})
        createUser(userData) 

#Function to create users on Intercom        
def createUser(userData):
    headers = { 'Content-Type': 'application/json', 'Authorization': 'Bearer <access_token>', 'Accept': 'application/json'}
    #Intercom accepts data in the form of json objects sent using post method and creates the user
    r = requests.post("https://api.intercom.io/users", data = userData, headers = headers) 
    
connectDb()


        





